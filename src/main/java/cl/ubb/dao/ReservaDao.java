package cl.ubb.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import cl.ubb.model.Reserva;

public interface ReservaDao	extends CrudRepository<Reserva,Long> {

	List<Reserva> findByClientAndDate(long id, String fecha);

	
}
