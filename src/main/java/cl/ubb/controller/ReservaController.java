package cl.ubb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import cl.ubb.model.Reserva;
import cl.ubb.service.ReservaService;

@RequestMapping("/reserva")
@RestController
public class ReservaController {

	@Autowired
	private ReservaService reservaService;
	
	@RequestMapping (value = "/reservarAuto", method = POST)
	@ResponseBody
	public ResponseEntity<Reserva>ReservarAuto(@RequestBody Reserva reserva){
			reservaService.reservarAuto(reserva);
			return new ResponseEntity<Reserva>(reserva, HttpStatus.CREATED);
	}
	@RequestMapping (value = "/listaReservas", method = GET)
	@ResponseBody
	public ResponseEntity<List<Reserva>>obtenerListaReservas(){
		List<Reserva> reservas = reservaService.obtenerListaReservasCliente();
		return new ResponseEntity<List<Reserva>>(reservas,HttpStatus.OK);
	}
	
}
