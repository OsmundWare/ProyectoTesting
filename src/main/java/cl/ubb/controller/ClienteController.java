package cl.ubb.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cl.ubb.model.Cliente;
import cl.ubb.service.ClienteService;

@RequestMapping("/cliente")
@RestController
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping (value = "/crearCliente", method = POST)
	@ResponseBody
	public ResponseEntity<Cliente>RegistrarCliente(@RequestBody Cliente cliente){
		clienteService.registrarCliente(cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.CREATED);
	}
	
	@RequestMapping (value = "/lista", method = GET)
	@ResponseBody
	public ResponseEntity<List<Cliente>>listarClientes(){
		List<Cliente> clientes = clienteService.listarClientes();
		return new ResponseEntity<List<Cliente>>(clientes,HttpStatus.OK);
	}
	
	@RequestMapping (value = "/consultaCliente/{rut}", method = GET)
	@ResponseBody
	public ResponseEntity<Cliente>obtenerClientePorRut(@PathVariable("rut") String rut){
		
		return new ResponseEntity<Cliente>(clienteService.obtenerClientePorRut(rut), HttpStatus.OK);
	}
	
	
}
