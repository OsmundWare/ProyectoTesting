package cl.ubb.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cl.ubb.model.Automovil;
import cl.ubb.service.AutomovilService;

@RequestMapping("/automovil")
@RestController
public class AutomovilController {
	
	@Autowired
	private AutomovilService automovilService;
	
	@RequestMapping (value = "/lista", method = GET)
	@ResponseBody
	public ResponseEntity<List<Automovil>>listarAutomovilesPorCategoria(){
		List<Automovil> Automoviles = automovilService.obtenerAutosPorCategoria("Lujo");
		return new ResponseEntity<List<Automovil>>(Automoviles,HttpStatus.OK);
	}

}
