package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Automovil {
	
	@Id
	@GeneratedValue
	
	private long id;
	private String marca;
	private String categoria;
	private String tipoTransmision;
	
	
	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String getMarca(){
		return marca;
	}
	
	public void setMarca(String marca){
		this.marca = marca;
	}
	
	public String getCategoria(){
		return categoria;
	}
	
	public void setCategoria(String categoria){
		this.categoria = categoria;
	}
	
	public String getTipoTransmision(){
		return tipoTransmision;
	}
	
	public void setTipoTransmision(String tipoTransmision){
		this.tipoTransmision = tipoTransmision;
	}
}
