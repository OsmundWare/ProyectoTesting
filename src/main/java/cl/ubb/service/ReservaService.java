package cl.ubb.service;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Cliente;
import cl.ubb.model.Reserva;


@Service
public class ReservaService {
	@Autowired
	private ReservaDao reservaDao;

	public Reserva reservarAuto(Reserva reserva) {
		return reservaDao.save(reserva);
	}

	public List<Reserva> obtenerListaReservasCliente() {
		List<Reserva> reservas = new ArrayList<Reserva>();
		reservas = (List<Reserva>) reservaDao.findByClientAndDate(anyLong(), anyString());
		
		return reservas;
	}
	
	
}
