package cl.ubb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ubb.dao.ClienteDao;
import cl.ubb.model.Cliente;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteDao clienteDao;
	
	
	public Cliente registrarCliente(Cliente cliente)
	{
		
		return clienteDao.save(cliente);
	}
	
	public List<Cliente> listarClientes()
	{
		List<Cliente> clientes = new ArrayList<Cliente>();
		clientes = (List<Cliente>)clienteDao.findAll();
		return clientes;
	}
	
public Cliente obtenerClientePorRut(String rut){
		
		Cliente cliente = new Cliente();
		cliente = clienteDao.findByRut(rut);
		
		if(cliente == null)
		{
			throw new  noExisteClienteExcepcion();
		}else{
			return cliente;
		}
		
		
	}
	
	public class noExisteClienteExcepcion extends RuntimeException{}

}
