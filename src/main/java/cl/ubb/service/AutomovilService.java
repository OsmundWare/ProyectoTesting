package cl.ubb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ubb.dao.AutomovilDao;
import cl.ubb.model.Automovil;

@Service
public class AutomovilService {

	@Autowired
	private AutomovilDao automovilDao;
	
	public List<Automovil> obtenerAutosPorCategoria(String categoria)
	{
		List<Automovil> automovil = new ArrayList<Automovil>();
		automovil = (List<Automovil>) automovilDao.findAutomovilByCategoria(categoria);
		return automovil;
	}
	
}
