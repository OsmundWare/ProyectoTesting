package cl.ubb.controller;

import static org.junit.Assert.*;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import cl.ubb.model.Cliente;
import cl.ubb.service.ClienteService;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;

@RunWith(MockitoJUnitRunner.class)
public class ClienteControllerTest {

	@Mock
	private ClienteService clienteService;
	
	@InjectMocks
	private ClienteController clienteController;

	@Test
	public void registrarcliente(){
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		cliente.setId(1);
		cliente.setRut("18069735-7");
		cliente.setNombre("Pablo Marmol");
		cliente.setCelular("133");
		cliente.setEmail("correo@email.cl");
		
		Mockito.when(clienteService.registrarCliente(Matchers.any(Cliente.class))).thenReturn(cliente);
		
		given().
		contentType("application/json").
		body(cliente).
		when().
		post("cliente/crearCliente").
		then().
		body("rut", equalTo(cliente.getRut())).
		statusCode(201);
	}
	
	@Test
	public void  ListarClientes(){
		RestAssuredMockMvc.standaloneSetup(clienteController);
		
		List<Cliente> clientes = new ArrayList<Cliente>();
		Cliente cliente1 = new Cliente();
		cliente1.setId(1);
		cliente1.setRut("18069735-7");
		cliente1.setNombre("Pablo Marmol");
		cliente1.setCelular("133");
		cliente1.setEmail("correo@email.cl");
		
		Cliente cliente2 = new Cliente();
		cliente2.setId(2);
		cliente2.setRut("9550477-9");
		cliente2.setNombre("jose");
		cliente2.setCelular("911");
		cliente2.setEmail("correo2@email.cl");
		
		clientes.add(cliente1);
		clientes.add(cliente2);
		
		
		Mockito.when(clienteService.listarClientes()).thenReturn(clientes);
		
		given().
		contentType("application/json").
		when().
		get("cliente/lista").
		then().
		body("id[0]", equalTo(1)).
		body("rut[0]", equalTo("18069735-7")).
		body("nombre[0]",equalTo("Pablo Marmol")).
		body("celular[0]", equalTo("133")).
		body("email[0]", equalTo("correo@email.cl")).
		statusCode(200);
		
	}
	
	@Test
	public void VerificarSiClienteEstaRegistrado(){
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		cliente.setRut("19806357-7");
		
		Mockito.when(clienteService.obtenerClientePorRut(anyString())).thenReturn(cliente);
		
		given().
			contentType("application/json").
		when().
			get("/cliente/consultaCliente/{rut}","19806357-7").
		then().
			body("rut", is("19806357-7")).
			statusCode(200);
		
	}
	
}
