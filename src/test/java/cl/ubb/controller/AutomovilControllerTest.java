package cl.ubb.controller;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import cl.ubb.model.Automovil;
import cl.ubb.service.AutomovilService;

@RunWith(MockitoJUnitRunner.class)
public class AutomovilControllerTest {
	
	@Mock
	private AutomovilService automovilService;
	
	@InjectMocks
	private AutomovilController automovilController;

	@Test
	public void ListarAutomovilesPorCategoria(){
		RestAssuredMockMvc.standaloneSetup(automovilController);
		List<Automovil> automoviles = new ArrayList<Automovil>();
		
		Automovil automovil1 = new Automovil();
		automovil1.setId(1);
		automovil1.setMarca("nissan");
		automovil1.setCategoria("Lujo");
		automovil1.setTipoTransmision("Mecanico");
		automoviles.add(automovil1);
		
		Mockito.when(automovilService.obtenerAutosPorCategoria("Lujo")).thenReturn(automoviles);
		
		given().
		contentType("application/json").
		when().
		get("automovil/lista").
		then().
		body("id[0]", equalTo(1)).
		body("marca[0]", equalTo("nissan")).
		body("categoria[0]",equalTo("Lujo")).
		body("tipoTransmision[0]", equalTo("Mecanico")).statusCode(200);
		
		
	}

}
