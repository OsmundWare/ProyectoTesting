package cl.ubb.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import cl.ubb.model.Reserva;
import cl.ubb.service.ReservaService;

@RunWith(MockitoJUnitRunner.class)
public class ReservaControllerTest {

	@Mock
	private ReservaService reservaService;
	@InjectMocks
	private ReservaController reservaController;
	
	@Test
	public void reservarAutomovil(){
		RestAssuredMockMvc.standaloneSetup(reservaController);
		Reserva reserva = new Reserva();
		reserva.setId(1);
		reserva.setFechaInicio("23-12-2016");
		reserva.setFechaFin("26-12-2016");
		reserva.setIdAutomovil(2);
		reserva.setIdCliente(3);
		
		Mockito.when(reservaService.reservarAuto(Matchers.any(Reserva.class))).thenReturn(reserva);
		
		given().
			contentType("application/json").
			body(reserva).
		when().
			post("reserva/reservarAuto").
		then().
			body("id", equalTo(1)).
			statusCode(201);
	}
	@Test
	public void listarReservasCliente(){
		RestAssuredMockMvc.standaloneSetup(reservaController);
		
		List<Reserva> listaReservas = new ArrayList<Reserva>();
		Reserva reserva = new Reserva();
		Reserva reservaDos = new Reserva();
		Reserva reservaTres = new Reserva();
		
		reserva.setId(1);
		reserva.setFechaInicio("23-12-2016");
		reserva.setFechaFin("26-12-2016");
		reserva.setIdAutomovil(2);
		reserva.setIdCliente(3);
		
		reservaDos.setId(2);
		reservaDos.setFechaInicio("24-12-2016");
		reservaDos.setFechaFin("31-12-2016");
		reservaDos.setIdAutomovil(5);
		reservaDos.setIdCliente(3);
		
		reservaTres.setId(3);
		reservaTres.setFechaInicio("31-12-2016");
		reservaTres.setFechaFin("01-01-2017");
		reservaTres.setIdAutomovil(7);
		reservaTres.setIdCliente(7);
		
		listaReservas.add(reserva);
		listaReservas.add(reservaDos);
		listaReservas.add(reservaTres);
		
		Mockito.when(reservaService.obtenerListaReservasCliente()).thenReturn(listaReservas);
		
		given().
		contentType("application/json").
		when().
		get("reserva/listaReservas").
		then().
		body("id[0]", equalTo(1)).
		body("fechaInicio[0]", equalTo("23-12-2016")).
		body("fechaFin[0]",equalTo("26-12-2016")).
		body("idAutomovil[0]", equalTo(2)).
		body("idCliente[0]", equalTo(3)).
		body("id[1]", equalTo(2)).
		body("fechaInicio[1]", equalTo("24-12-2016")).
		body("fechaFin[1]",equalTo("31-12-2016")).
		body("idAutomovil[1]", equalTo(5)).
		body("idCliente[1]", equalTo(3)).
		statusCode(200);
	}

}
