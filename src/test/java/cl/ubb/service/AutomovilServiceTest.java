package cl.ubb.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.AutomovilDao;
import cl.ubb.model.Automovil;


@RunWith(MockitoJUnitRunner.class)
public class AutomovilServiceTest {
	
	@Mock
	private AutomovilDao automovilDao;
	
	@InjectMocks
	private AutomovilService automovilService;

	@Test
	public void listarAutosPorCategoria()
	{
		//arrange
		List<Automovil> automovil = new ArrayList<Automovil>();
		List<Automovil> resultado = new ArrayList<Automovil>();
		//act
		when(automovilDao.findAutomovilByCategoria(anyString())).thenReturn(automovil);
		resultado = automovilService.obtenerAutosPorCategoria("Lujo");
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(automovil, resultado);
	}



}
