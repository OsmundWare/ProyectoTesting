package cl.ubb.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ClienteDao;
import cl.ubb.model.Cliente;
import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class ClienteServiceTest {
	
	@Mock
	private ClienteDao clienteDao;
	
	@InjectMocks
	private ClienteService clienteService;
	
	@Test
	public void cuandoRegistroUnClienteRetornaElmismo()
	{
		//arrange
			Cliente cliente = new Cliente();
			cliente.setId(1L);
			Cliente resultado = new Cliente();
		
		//act
			
			when(clienteDao.save(cliente)).thenReturn(cliente);
			resultado = clienteService.registrarCliente(cliente);
		
		//assert
			Assert.assertNotNull(resultado);
			Assert.assertEquals(resultado.getId(), 1L);
		
	}
	
	@Test
	public void retornarListaClientes() throws Exception
	{
		//arrange
		List<Cliente> cliente = new ArrayList<Cliente>();
		List<Cliente> resultados = new ArrayList<Cliente>();
		//act
			when(clienteDao.findAll()).thenReturn(cliente);
			resultados = clienteService.listarClientes();
		//assert
		
			Assert.assertNotNull(resultados);
			Assert.assertEquals(resultados, cliente);
		
	}
	
	@Test(expected = ClienteService.noExisteClienteExcepcion.class) //assert
	public void lanzarExcepcionSiClienteNoEstaRegistrado() throws Exception{
		
		// act 
		when(clienteDao.findByRut("18069735-7")).thenReturn(null);
		clienteService.obtenerClientePorRut("9550477-9");
		
	}

}
