package cl.ubb.service;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import org.junit.Assert;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Cliente;
import cl.ubb.model.Reserva;

@RunWith(MockitoJUnitRunner.class)
public class ReservaServiceTest {
	
	@Mock
	private ReservaDao reservaDao;
	
	@InjectMocks
	private ReservaService reservaService;
	
	@Test
	public void reservarAutomovil(){
		//arrange
		Reserva reserva = new Reserva();		
		Reserva resultado = new Reserva();
		//act
		when(reservaDao.save(reserva)).thenReturn(reserva);
		resultado = reservaService.reservarAuto(reserva);
		//assert
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(reserva,resultado);
	}
	
	@Test
	public void devuelveListaReservasDelClienteEnUnaFecha(){
		//Arrange
		List<Reserva> listaReserva = new ArrayList<Reserva>();
		List<Reserva> listaPrueba = new ArrayList<Reserva>();
		
		//Act
		when(reservaDao.findByClientAndDate(anyLong(),anyString())).thenReturn(listaReserva);
		listaPrueba = reservaService.obtenerListaReservasCliente();
		
		//Assert
		Assert.assertNotNull(listaPrueba);
		Assert.assertEquals(listaReserva, listaPrueba);
	}

}
